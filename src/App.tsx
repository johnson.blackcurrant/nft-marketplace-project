import "./App.css";
import CreateAndSell from "./components/CreateAndSell";
import EventCollaborated from "./components/EventCollaborated";
import ExploreCategory from "./components/ExploreCategory";
import Footer from "./components/Footer";
import MailingList from "./components/MailingList";
import NFTProduct from "./components/NFTProduct";
import TopCreator from "./components/TopCreator";
import TrendingProduct from "./components/TrendingProduct";
import GetStarted from "./components/GetStarted";
import Navbar from "./components/Navbar";

function App() {
  return (
    <div className="App">
      {/* <h1 className="display-4">Johnson</h1>
      <h1>Johnson</h1> */}
      <Navbar />
      <GetStarted />
      <TrendingProduct />
      <CreateAndSell />
      <ExploreCategory />
      <TopCreator />
      <NFTProduct />
      <EventCollaborated />

      <MailingList />
      <Footer />
    </div>
  );
}

export default App;
