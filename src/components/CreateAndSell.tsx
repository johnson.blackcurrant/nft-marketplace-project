import React from "react";
import { IoCreateOutline } from "react-icons/io5";
import { IoCameraOutline } from "react-icons/io5";
import { AiFillDollarCircle } from "react-icons/ai";

const CreateAndSell = () => {
  return (
    <div>
      <div className="container  createAndSellPaddingTop">
        <div className="text-center">
          <h1 className="text-light createSellFont">
            Create and Sell NFT Product
          </h1>
          <br />
          <h5 className=" letsCreateFont">
            Let's create and sell your digital product and <br /> we help you
            sell them{" "}
          </h5>
        </div>
        <div className="row contentPaddingTop justify-content-between g-5">
          <div className="col-md-6 col-sm-12 col-lg-4  text-center justify-content-start">
            <IoCreateOutline className="blueTheme" size={85} />
            <br />
            <br />
            <h2 className="text-light font-weight-light  createSellHeadingFont">
              Create your product
            </h2>
            <br />
            <h5 className=" belowNumberFont createSellSmallFont">
              Create and setup your product <br /> (image, video, music, art).
              Add <br /> social links. a description
            </h5>
          </div>
          <div className="col col-md-6 col-sm-12 col-lg-4 text-center">
            <IoCameraOutline className="blueTheme" size={85} />
            <br />
            <br />
            <h2 className="text-light font-weight-light createSellHeadingFont">
              Upload your work
            </h2>
            <br />
            <h5 className=" belowNumberFont createSellSmallFont">
              Upload your work. Add social links, a <br /> description, profile
              & banner images, <br /> and set a secondary sales fee.
            </h5>
          </div>
          <div className="col col-md-6 col-sm-12 col-lg-4 text-center">
            <AiFillDollarCircle className="blueTheme" size={85} />
            <br />
            <br />
            <h2 className="text-light font-weight-light createSellHeadingFont">
              Set up collection
            </h2>
            <br />
            <h5 className=" belowNumberFont createSellSmallFont">
              Create and setup your product <br /> (image, video, music, art).
              Add <br /> social links. a description
            </h5>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreateAndSell;
