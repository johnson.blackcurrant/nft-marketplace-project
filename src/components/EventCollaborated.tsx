import React from "react";
import { GoVerified } from "react-icons/go";

const EventCollaborated = () => {
  return (
    <div className="container containerPadding">
      {/* left div */}
      <div className="row g-4">
        <div className="col-md-12 col-sm-12 col-lg-8 ps-5">
          <h1 className="text-light createSellFont">
            Event collaborated digital <br /> artist in Tokyo{" "}
          </h1>
          <br />
          <h5 className=" nftProductNewsParaFont">
            At this event. all creators or digital artists will collaborate with
            each <br />
            other to create a product that is very cool and full of aesthetics.
            so <br />
            don't let you miss this event
          </h5>
          <br />
          <h5 className="benefitFont">The benefits you will get</h5>
          <br />
          <div className="row w-75 ">
            <div className="col-md-6 col-sm-12 col-lg-5">
              <h5 className="text-light benefitsElementFont">
                <GoVerified className="blueTheme me-2" />
                Merchandise Special
              </h5>
              <br />
              <h5 className="text-light benefitsElementFont">
                <GoVerified className="blueTheme me-2" />
                Launch and lodging
              </h5>
              <br />
              <h5 className="text-light benefitsElementFont">
                <GoVerified className="blueTheme me-2" />
                T-Shirt Event
              </h5>
              <br />
            </div>
            <div className="col-md-6 col-sm-12 col-lg-5">
              <h5 className="text-light benefitsElementFont">
                <GoVerified className="blueTheme me-2" />
                Free Creator tool
              </h5>
              <br />
              <h5 className="text-light benefitsElementFont">
                <GoVerified className="blueTheme me-2" />
                Gift for the lucky one
              </h5>
            </div>
          </div>
        </div>
        {/* left div end */}
        {/* card div start */}
        <div className="col-md-12 col-sm-12 col-lg-4 d-flex justify-content-sm-center justify-content-md-center">
          <div className="card ">
            <img
              src="../paint-16.jpg"
              alt=""
              className="eventCollaboratedCardImage"
            />
            {/* <div className=" text-center"> */}
            <div className="container eventCollaboratedCardBg">
              <p className="pt-3 eventCollaboratedP">Limited Seat</p>
              <h4 className="eventCollaboratedCardTitle text-light">
                Tokyo Digital Art Hunter
              </h4>
              <p className="eventCollaboratedP2">See you in Tokyo 3.00 PM</p>
              <div className="text-center pb-3">
                <button className=" joinEventButton">Join&nbsp;Event</button>
              </div>
            </div>
          </div>
          {/* card div end */}
        </div>
      </div>
    </div>
    // </div>
  );
};

export default EventCollaborated;
