import React from "react";

const ExploreCategory = () => {
  return (
    <div>
      <div className="container  containerPadding">
        <div className="text-center">
          <h1 className="text-light createSellFont">Explore by category</h1>
          <br />
          <h5 className=" letsCreateFont">
            Let's create and sell your digital product and <br /> we help you
            sell them{" "}
          </h5>
        </div>
        <div className="contentPaddingTop">
          <div className="row justify-content-evenly g-4">
            <div className="col-3 exploreCategoryElements">
              {/* <img src="../../paint-2.jpg" alt="" /> */}
              <h4 className="text-light fontInsideExploreCategoryCard">Art</h4>
            </div>
            <div className="col-3 exploreCategoryElement2">
              {/* <img src="../../paint-2.jpg" alt="" /> */}
              <h4 className="text-light fontInsideExploreCategoryCard">
                Abstract
              </h4>
            </div>
            <div className="col-3 exploreCategoryElement3">
              <h4 className="text-light fontInsideExploreCategoryCard">
                Sport
              </h4>
              {/* <img src="../../paint-2.jpg" alt="" /> */}
            </div>
          </div>
          <br />
          <div className="row justify-content-evenly g-4">
            <div className="col-3 exploreCategoryElement4">
              {/* <img src="../../paint-2.jpg" alt="" /> */}
              <h4 className="text-light fontInsideExploreCategoryCard">
                Ai World
              </h4>
            </div>
            <div className="col-3 exploreCategoryElement5">
              {/* <img src="../../paint-2.jpg" alt="" /> */}
              <h4 className="text-light fontInsideExploreCategoryCard">
                Collection
              </h4>
            </div>
            <div className="col-3 exploreCategoryElement6">
              <h4 className="text-light fontInsideExploreCategoryCard">
                All Product
              </h4>
              {/* <img src="../../paint-2.jpg" alt="" /> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ExploreCategory;
