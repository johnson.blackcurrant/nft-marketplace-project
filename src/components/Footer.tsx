import React from "react";
import { GiArrowWings } from "react-icons/gi";
import { FaFacebookF } from "react-icons/fa";
import { AiFillYoutube } from "react-icons/ai";
import { AiOutlineTwitter } from "react-icons/ai";
import { AiOutlineInstagram } from "react-icons/ai";

const Footer = () => {
  return (
    <div className="container containerPadding">
      <div className="row g-5">
        <div className="col-md-6 col-sm-12 col-lg-4 text-sm-center text-md-start text-lg-start">
          <h1 className="text-bold text-light ">
            {" "}
            <GiArrowWings className="blueTheme " /> InDart
          </h1>
          <br />
          <p className="text-light footerPara">
            The world's first and largest digital marketplace for crypto <br />{" "}
            collectibles and non-fungible tokens (NFTs), buy, sell, and <br />
            discover exclusive digital assests.
          </p>
          <br />
          <div className="row w-75 justify-content-md-center justify-content-lg-start justify-content-sm-center">
            <div className="col-2">
              <FaFacebookF className="socialMediaIcons" size={20} />
            </div>
            <div className="col-2">
              <AiFillYoutube className="socialMediaIcons" size={20} />
            </div>
            <div className="col-2">
              <AiOutlineTwitter className="socialMediaIcons" size={20} />
            </div>
            <div className="col-2">
              <AiOutlineInstagram className="socialMediaIcons" size={20} />
            </div>
          </div>
        </div>
        <div className="col-md-6 col-sm-12 col-lg-8">
          <div className="row ps-5">
            <div className="col-md-3  col-lg-3 d-flex justify-content-end">
              <div>
                <h4 className="text-light footerHeadingFont">Products</h4>
                <div className="footerSmallFonts pt-3">
                  <p>Platform</p>
                  <p>Wallet</p>
                  <p>Marketplace</p>
                  <p>Ranking</p>
                  <p>Security</p>
                </div>
              </div>
            </div>
            <div className="col-3 d-flex justify-content-end">
              <div>
                <h4 className="text-light footerHeadingFont">Resources</h4>
                <div className="footerSmallFonts pt-3">
                  <p>Download</p>
                  <p>Blog</p>
                  <p>Events</p>
                  <p>Partner</p>
                </div>
              </div>
            </div>
            <div className="col-3 d-flex justify-content-end">
              <div>
                <h4 className="text-light footerHeadingFont">About</h4>
                <div className="footerSmallFonts pt-3">
                  <p>About us</p>
                  <p>Contact us</p>
                  <p>community</p>
                  <p>Carrers</p>
                  <p>FAQ</p>
                </div>
              </div>
            </div>
            <div className="col-3 d-flex justify-content-end">
              <div>
                <h4 className="text-light footerHeadingFont">Help</h4>
                <div className="footerSmallFonts pt-3">
                  <p>Help Center</p>
                  <p>Contact us</p>
                  <p>Product Status</p>
                  <p>Login</p>
                  <p>Sign Up</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
