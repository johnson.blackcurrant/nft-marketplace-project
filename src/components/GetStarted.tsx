import { GoVerified } from "react-icons/go";

const GetStarted = () => {
  return (
    <div className="container getStartedPaddingTop ">
      <div className="row align-items-center g-3">
        <div className="col-md-12 col-sm-12 col-lg-6 ">
          <h1 className=" text-light getStartedTitle">
            Discover{" "}
            <span className="blueTheme">
              Unique & <br /> Rare
            </span>{" "}
            Digital Art
          </h1>
          <br />
          <br />
          <h6 className="getStartedPara">
            Create a beautiful NFT Product. Explore the best
            <br /> collection from popular digital artists.
          </h6>
          <br />
          <br />
          <div className="row">
            <div className="col-4">
              <button className="getStartedButton">
                {" "}
                <span>Get&nbsp;Started</span>{" "}
              </button>
            </div>
            <div className="col ps-5">
              <button className="createButton">Create</button>
            </div>
          </div>
          <br />
          <br />
          <div className="row">
            <div className="col-4">
              <h4 className="light text-light getStartedNumber">25M+</h4>
              <h6 className="belowNumberFont">Exclusive Product</h6>
            </div>
            <div className="col">
              <h4 className="text-light getStartedNumber">10k+</h4>
              <h6 className="  belowNumberFont">Digital Artist</h6>
            </div>
          </div>
        </div>

        <div className="col-md-3 col-sm-12 col-lg-6">
          <h1 className="getStartedTitle2 pb-3">
            Super <span className="blueTheme"> Rare Art </span>
          </h1>
          <div className="row justify-content-around">
            {/* <div className="col-3"> */}
            {/* <div className="row"> */}
            <div className="col-md-2 col-sm-8 col-lg-6">
              <div className="card">
                <img
                  src="../getStartedPic1.jpg"
                  alt=""
                  className=" getStartedCardImg1"
                />

                <div className="container getStartedCardBg">
                  <div className="row ps-4 pt-4 align-items-center justify-content-center align-self-center">
                    <div className="col-3 mb-2 ">
                      <img
                        className="cardProfilePic Profile-1  "
                        src="../../Profile-9.jpg"
                        alt=""
                      />
                    </div>
                    <div className="col-8 ps-4">
                      <h5 className="text-light mb-1 cardTitle">Aqua Effect</h5>
                      <p className=" cardProfile">
                        by Bella Noviac{" "}
                        <GoVerified className="blueTheme " size={10} />
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-2 col-lg-6 mt-sm-4 mt-md-0 mt-lg-0">
              <div className="card">
                <img
                  src="../getStartedPic2.jpg"
                  alt=""
                  className=" getStartedCardImg2"
                />

                <div className="container getStartedCardBg">
                  <div className="row ps-4 pt-4 align-items-center justify-content-center align-self-center">
                    <div className="col-3 mb-2 ">
                      <img
                        className="cardProfilePic Profile-1  "
                        src="../../Profile-10.jpg"
                        alt=""
                      />
                    </div>
                    <div className="col-8 ps-4">
                      <h5 className="text-light mb-1 cardTitle">Fire Hearts</h5>
                      <p className=" cardProfile">
                        by Andre Swaster{" "}
                        <GoVerified className="blueTheme " size={10} />
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <img
                  src="../getStartedPic3.jpg"
                  alt=""
                  className=" getStartedCardImg2 mt-5"
                />
                <div className="getStartedRelativeBox">
                  <div className="row pt-4 flex">
                    <div className="col-1 ">
                      <img
                        className="cardProfilePic Profile-1"
                        src="../../Profile-12.jpg"
                        alt=""
                      />
                    </div>
                    <div className="col-8 ps-5  ">
                      <h5 className="text-light mb-1 cardTitle text-center ">
                        Angel Bird
                      </h5>
                      <p className=" cardProfile ps-4">
                        by Jenita Jane{" "}
                        <GoVerified className="blueTheme " size={10} />
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* </div> */}
            {/* </div> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default GetStarted;
