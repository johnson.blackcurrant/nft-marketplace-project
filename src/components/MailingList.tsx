import React from "react";
import { AiOutlineMail } from "react-icons/ai";

const MailingList = () => {
  return (
    <div className="containerPadding">
      <div className="mailingListBg ">
        <div className="container   containerPadding ">
          <div className="row d-flex align-items-center g-3">
            <div className="col-md-12 col-sm-12 col-lg-6 text-sm-center text-lg-start ">
              <h1 className="text-light  ">Join Our Mailing list</h1>
              <br />
              <p className="text-light mailingListP">
                Just insert your email into the field below. And you will get{" "}
                <br />
                the updates about fashion & styles from us.
              </p>
            </div>
            <div className="col-md-12 col-sm-12 col-lg-6">
              <div className="input-group mb-3">
                {/* <AiOutlineMail className="mailingListIcon" /> */}
                <input
                  type="text"
                  className="form-control"
                  placeholder="Your Email"
                  aria-label="Recipient's username"
                  aria-describedby="basic-addon2"
                />

                <button
                  className="input-group-text mailingListButton"
                  id="basic-addon2"
                >
                  Subscibe
                </button>
                {/* <span className="input-group-text" id="basic-addon2">
              Subscibe
            </span> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MailingList;
