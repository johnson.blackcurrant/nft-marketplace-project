import React from "react";
import { HiArrowSmRight } from "react-icons/hi";
import { GoVerified } from "react-icons/go";

const NFTProduct = () => {
  return (
    <div>
      <div className="container  containerPadding">
        <div className="row ">
          <div className="col-6">
            <h1 className="text-light  createSellFont">NFT Product News</h1>
          </div>
          <div className="col-6 ">
            <div className="text-light text-right d-flex NFTProducth4  justify-content-end">
              <h4 className="NFTProducth4">See All Our Newslater</h4>

              <HiArrowSmRight size={28} className="aiOutlineArrowRightIcon" />
            </div>
          </div>
        </div>

        {/* <div className="text-center"> */}

        {/* </div> */}

        <div className="row contentPaddingTop g-5">
          <div className="col-md-12 col-sm-12 col-lg-4 d-flex justify-content-sm-center">
            {/* card start */}
            <div className="card nftProductCardWidth">
              <img
                src="../paint-15.jpg"
                alt=""
                className="eventCollaboratedCardImage"
              />
              {/* <div className=" text-center"> */}
              <div className="container nftProductCardBg">
                <form className="d-flex pt-4 pb-4">
                  {/* <BiSearch /> */}
                  <input
                    className="nftProductFormControl  "
                    type="search"
                    placeholder="Search"
                    aria-label="Search"
                  />
                  {/* <button className="btn btn-outline-success" type="submit">
                Search
              </button> */}
                </form>
                <div className="row">
                  <div className="col-6 text-light ">
                    <h5 className="nftProductCardHeading">Bumble Abstract</h5>
                  </div>
                  <div className="col-6 d-flex justify-content-end">
                    <GoVerified className="blueTheme " size={15} />
                  </div>
                </div>

                <br />
                <div className="row">
                  <div className="col-6 ">
                    <h5 className="nftProductCardHeading">Art Abstract</h5>
                  </div>
                  <div className="col-6 d-flex justify-content-end">
                    <GoVerified className="blueTheme " size={15} />
                  </div>
                </div>

                <br />
                {/* <h5 className="text-light ps-4">
                  Absolute Art{" "}
                  <GoVerified
                    className="createSellIcon nftProductVerifiedPosition"
                    size={15}
                  />
                </h5> */}
                <div className="row ">
                  <div className="col-6 text-light">
                    <h5 className="nftProductCardHeading">Absolute Art</h5>
                  </div>
                  <div className="col-6 d-flex justify-content-end">
                    <GoVerified className="blueTheme " size={15} />
                  </div>
                </div>

                {/* <h4 className="tokyoFont text-light">
                  Tokyo Digital Art Hunter
                </h4>
                <p className="eventCollaboratedP2">See you in Tokyo 3.00 PM</p>
                <div className="text-center pb-3">
                  <button className=" joinEventButton">Join Event</button>
                </div> */}
              </div>
            </div>
            {/* card end */}
          </div>
          {/* right container start */}
          <div className="col-md-12 col-sm-12 col-lg-7">
            <h3 className="text-light createSellFont text-sm-center text-md-start ">
              You can explore more <br /> than 25M Digital Product{" "}
            </h3>
            <br />
            <h5 className="letsCreateFont nftProductNewsParaFont text-sm-center text-md-start">
              You can choose one of the many options we have on Our Website.
              Each <br />
              product has been rated 4+ stars by our previous users. You can
              search by <br />
              clicking our button below this text
            </h5>
            <br />
            <br />
            <div className="d-flex justify-content-sm-center justify-content-md-start">
              <button className="createButton ">Explore</button>
            </div>
          </div>
          {/* right container end */}
        </div>
      </div>
    </div>
  );
};

export default NFTProduct;
