import React from "react";
import { GiArrowWings } from "react-icons/gi";
import { BiSearch } from "react-icons/bi";
import { BsFillBellFill } from "react-icons/bs";

const Navbar = () => {
  return (
    <div className="pb-5 ">
      <nav className="navbar py-4 navbar-expand-lg navbar-light fixed-top  navbarBgColor  ">
        <div className="container-fluid container">
          <h1 className="text-thin text-light navbarIndart">
            {" "}
            <GiArrowWings className="blueTheme " size={25} /> InDart
          </h1>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarToggleExternalContent"
            aria-controls="navbarToggleExternalContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
              <li className="nav-item me-5">
                <a
                  className="nav-link active text-light navbaraTag"
                  aria-current="page"
                  href="#"
                >
                  Marketplace
                </a>
              </li>
              <li className="nav-item me-5">
                <a className="nav-link text-light navbaraTag" href="#">
                  Ranking
                </a>
              </li>
              <li className="nav-item me-5">
                <a
                  className="nav-link disabled text-light navbaraTag"
                  href="#"
                  //   tabindex="-1"
                  aria-disabled="true"
                >
                  Creator
                </a>
              </li>
              <form className="d-flex ">
                <BiSearch />
                <input
                  className="form-control me-3"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                {/* <button className="btn btn-outline-success" type="submit">
                Search
              </button> */}
              </form>
              <div className="bellOutline me-3 ">
                <BsFillBellFill className="bellIcon " size={20} />
              </div>
              <button className="d-flex uploadButton">Upload</button>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
