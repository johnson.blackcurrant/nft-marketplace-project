import React from "react";
import { GoVerified } from "react-icons/go";

const TopCreator = () => {
  return (
    <div>
      <div className="container  containerPadding">
        <div className="text-center">
          <h1 className="text-light createSellFont">Top Creator</h1>
          <br />
          <h5 className=" letsCreateFont">
            The following is a row of creators who have the
            <br /> highest points in sales
          </h5>
        </div>
        {/* card start */}
        <div className="row contentPaddingTop justify-content-evenly g-5 ">
          <div className="col-md-6 col-sm-12 col-lg-2">
            <div className="card topCreatorCardWidth">
              <img
                src="../Profile-11.jpg"
                alt=""
                className=" TopCreatorCardImg"
              />
              <div className="card-body text-center">
                <h5 className="card-title text-light topCreatorName ">
                  Sisca Ria <GoVerified className="blueTheme " size={10} />
                </h5>

                <p className="card-text  topCreatorPoints">108823 Points</p>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-lg-2">
            <div className="card topCreatorCardWidth">
              <img
                src="../Profile-13.jpg"
                alt=""
                className="TopCreatorCardImg"
              />
              <div className="card-body text-center">
                <h5 className="card-title text-light topCreatorName ">
                  John Cena <GoVerified className="blueTheme " size={10} />
                </h5>

                <p className="card-text  topCreatorPoints">108823 Points</p>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-lg-2">
            <div className="card topCreatorCardWidth">
              <img
                src="../Profile-10.jpg"
                alt=""
                className="TopCreatorCardImg"
              />
              <div className="card-body text-center">
                <h5 className="card-title text-light topCreatorName ">
                  Andre Swaster
                  <GoVerified className="blueTheme ms-1" size={10} />
                </h5>

                <p className="card-text  topCreatorPoints">108823 Points</p>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-lg-2">
            <div className="card topCreatorCardWidth">
              <img
                src="../Profile-12.jpg"
                alt=""
                className="TopCreatorCardImg"
              />
              <div className="card-body text-center">
                <h5 className="card-title text-light topCreatorName ">
                  Jenita Jane
                  <GoVerified className="blueTheme ms-1" size={10} />
                </h5>

                <p className="card-text  topCreatorPoints">108823 Points</p>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-lg-2">
            <div className="card topCreatorCardWidth">
              <img
                src="../Profile-9.jpg"
                alt=""
                className="TopCreatorCardImg"
              />
              <div className="card-body text-center">
                <h5 className="card-title text-light topCreatorName ">
                  Bella Noviac
                  <GoVerified className="blueTheme ms-1" size={10} />
                </h5>

                <p className="card-text  topCreatorPoints">108823 Points</p>
              </div>
            </div>
          </div>
        </div>
        {/* cards end */}
      </div>
    </div>
  );
};

export default TopCreator;
