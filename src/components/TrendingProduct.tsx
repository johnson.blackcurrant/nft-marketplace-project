import React from "react";
import { HiArrowSmRight } from "react-icons/hi";
import { GoVerified } from "react-icons/go";
import { MdTimer } from "react-icons/md";
import { FaEthereum } from "react-icons/fa";

const TrendingProduct = () => {
  return (
    <div>
      <div className="container  trendingProductPaddingTop">
        <div className="text-left">
          <h1 className="text-light createSellFont">Trending Product</h1>
        </div>
        <br />
        <div className="row ">
          <div className="col-6">
            <h1 className="text-light  letsCreateFont">
              The best NFT product of the week
            </h1>
          </div>
          <div className="col-6 ">
            <div className="text-light text-right d-flex NFTProducth4  justify-content-end pe-3">
              <h4 className="NFTProducth4">See All Product</h4>

              <HiArrowSmRight size={28} className="aiOutlineArrowRightIcon" />
            </div>
          </div>
        </div>

        {/* <div className="text-center"> */}

        {/* </div> */}
        <br />
        <div className="row">
          <div className="col-md-6 col-lg-4 pb-sm-5 pb-lg-0">
            <div className="card trendingProductCard">
              <img
                src="../trendingProductImage-1.jpg"
                alt=""
                className="eventCollaboratedCardImage"
              />
              {/* <div className=" text-center"> */}

              <div className="container trendingProductCardBg">
                <div className="row pt-3">
                  <div className="col-2 ">
                    <img
                      className="cardProfilePic Profile-1 "
                      src="../../Profile-1.jpg"
                      alt=""
                    />
                  </div>
                  <div className="col-8 ps-4">
                    <h5 className="text-light mb-1 cardTitle">Sea Princess</h5>
                    <p className=" cardProfile">
                      by Julia Mordova{" "}
                      <GoVerified className="blueTheme " size={10} />
                    </p>
                  </div>
                </div>

                <div className="row">
                  <div className="col-7">
                    <p className=" aboveNumberFont"> Current end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <MdTimer className="blueTheme " /> 31:12
                    </h4>
                  </div>

                  <div className="col pb-2">
                    <p className="aboveNumberFont"> Auction end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <FaEthereum className="blueTheme " size={20} /> 0.80 ETH
                    </h4>
                  </div>
                </div>

                <div className="text-center pb-3">
                  <button className="placeBidButton">
                    Place&nbsp;a&nbsp;Bid
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6  col-lg-4 pb-sm-5 pb-lg-0">
            <div className="card">
              <img
                src="../paint-10.jpg"
                alt=""
                className="eventCollaboratedCardImage"
              />
              {/* <div className=" text-center"> */}

              <div className="container trendingProductCardBg">
                <div className="row pt-3">
                  <div className="col-2 ">
                    <img
                      className="cardProfilePic Profile-1 "
                      src="../../Profile-14.jpg"
                      alt=""
                    />
                  </div>
                  <div className="col-8 ps-4">
                    <h5 className="text-light mb-1 cardTitle">
                      Spectacles Effect
                    </h5>
                    <p className=" cardProfile">
                      by Angelina Joe{" "}
                      <GoVerified className="blueTheme " size={10} />
                    </p>
                  </div>
                </div>

                <div className="row  ">
                  <div className="col-7">
                    <p className=" aboveNumberFont"> Current end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <MdTimer className="blueTheme " /> 40:56
                    </h4>
                  </div>

                  <div className="col pb-2">
                    <p className="aboveNumberFont"> Auction end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <FaEthereum className="blueTheme " size={20} /> 0.78 ETH
                    </h4>
                  </div>
                </div>

                <div className="text-center pb-3">
                  <button className="placeBidButton">
                    Place&nbsp;a&nbsp;Bid
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-lg-4 pb-sm-5 pb-lg-0">
            <div className="card">
              <img
                src="../paint-11.jpg"
                alt=""
                className="eventCollaboratedCardImage"
              />
              {/* <div className=" text-center"> */}

              <div className=" container trendingProductCardBg">
                <div className="row pt-3">
                  <div className="col-2 ">
                    <img
                      className="cardProfilePic Profile-1 "
                      src="../../Profile-13.jpg"
                      alt=""
                    />
                  </div>
                  <div className="col-8 ps-4">
                    <h5 className="text-light mb-1 cardTitle">
                      Abstract Paint
                    </h5>
                    <p className=" cardProfile">
                      by John Cena{" "}
                      <GoVerified className="blueTheme " size={10} />
                    </p>
                  </div>
                </div>

                <div className="row  ">
                  <div className="col-7">
                    <p className=" aboveNumberFont"> Current end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <MdTimer className="blueTheme " /> 48:08
                    </h4>
                  </div>

                  <div className="col pb-2">
                    <p className="aboveNumberFont"> Auction end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <FaEthereum className="blueTheme " size={20} /> 0.98 ETH
                    </h4>
                  </div>
                </div>

                <div className="text-center pb-3">
                  <button className="placeBidButton">
                    Place&nbsp;a&nbsp;Bid
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row  pt-4">
          <div className="col-md-6 col-lg-4 pb-sm-5 pb-lg-0">
            <div className="card">
              <img
                src="../paint-12.jpg"
                alt=""
                className="eventCollaboratedCardImage"
              />
              {/* <div className=" text-center"> */}

              <div className="container trendingProductCardBg">
                <div className="row pt-3">
                  <div className="col-2 ">
                    <img
                      className="cardProfilePic Profile-1 "
                      src="../../Profile-11.jpg"
                      alt=""
                    />
                  </div>
                  <div className="col-8 ps-4">
                    <h5 className="text-light mb-1 cardTitle">Water Leaf</h5>
                    <p className=" cardProfile">
                      by Sisca Ria{" "}
                      <GoVerified className="blueTheme " size={10} />
                    </p>
                  </div>
                </div>

                <div className="row  ">
                  <div className="col-7">
                    <p className=" aboveNumberFont"> Current end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <MdTimer className="blueTheme " /> 42:24
                    </h4>
                  </div>

                  <div className="col pb-2">
                    <p className="aboveNumberFont"> Auction end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <FaEthereum className="blueTheme " size={20} /> 1.26 ETH
                    </h4>
                  </div>
                </div>

                <div className="text-center pb-3">
                  <button className="placeBidButton">
                    Place&nbsp;a&nbsp;Bid
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6  col-lg-4 pb-sm-5 pb-lg-0">
            <div className="card">
              <img
                src="../paint-13.jpg"
                alt=""
                className="eventCollaboratedCardImage"
              />
              {/* <div className=" text-center"> */}

              <div className="container trendingProductCardBg">
                <div className="row pt-3">
                  <div className="col-2 ">
                    <img
                      className="cardProfilePic Profile-1 "
                      src="../../Profile-15.jpg"
                      alt=""
                    />
                  </div>
                  <div className="col-8 ps-4">
                    <h5 className="text-light mb-1 cardTitle">Water Bender</h5>
                    <p className=" cardProfile">
                      by Jessica Milla{" "}
                      <GoVerified className="blueTheme " size={10} />
                    </p>
                  </div>
                </div>

                <div className="row  ">
                  <div className="col-7">
                    <p className=" aboveNumberFont"> Current end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <MdTimer className="blueTheme " /> 54:39
                    </h4>
                  </div>

                  <div className="col pb-2">
                    <p className="aboveNumberFont"> Auction end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <FaEthereum className="blueTheme " size={20} /> 1.48 ETH
                    </h4>
                  </div>
                </div>

                <div className="text-center pb-3">
                  <button className="placeBidButton">
                    Place&nbsp;a&nbsp;Bid
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6  col-lg-4 pb-sm-5 pb-lg-0">
            <div className="card">
              <img
                src="../paint-14.jpg"
                alt=""
                className="eventCollaboratedCardImage"
              />
              {/* <div className=" text-center"> */}

              <div className="container trendingProductCardBg">
                <div className="row pt-3">
                  <div className="col-2 ">
                    <img
                      className="cardProfilePic Profile-1 "
                      src="../../Profile-16.jpg"
                      alt=""
                    />
                  </div>
                  <div className="col-8 ps-4">
                    <h5 className="text-light mb-1 cardTitle">Sea Princess</h5>
                    <p className=" cardProfile">
                      by Robert Lewan{" "}
                      <GoVerified className="blueTheme " size={10} />
                    </p>
                  </div>
                </div>

                <div className="row  ">
                  <div className="col-7">
                    <p className=" aboveNumberFont"> Current end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <MdTimer className="blueTheme " /> 57:41
                    </h4>
                  </div>

                  <div className="col pb-2">
                    <p className="aboveNumberFont"> Auction end</p>
                    <h4 className="text-light trendingProductNumbers">
                      {" "}
                      <FaEthereum className="blueTheme " size={20} /> 0.75 ETH
                    </h4>
                  </div>
                </div>

                <div className="text-center pb-3">
                  <button className="placeBidButton">
                    Place&nbsp;a&nbsp;Bid
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TrendingProduct;
